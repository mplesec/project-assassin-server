var Boom   = require('boom');


module.exports = {

    findWeapon : function(request, reply) {

        var Weapon = request.models.Weapon;


        var id = request.params.id;

        if (!id) {

            // Return all weapons
            Weapon.findAll().then(function(weapons) {

                return reply(weapons).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        } else {

            id = encodeURIComponent(request.params.id);

            // Return specific weapon
            Weapon.findOne({

                where : {
                    id : id
                }

            }).then(function(weapon) {

                if (!weapon) {

                    return reply(
                        Boom.notFound('Weapon not found')
                    );

                }

                return reply(weapon).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }

    },

    createWeapon : function(request, reply) {

        var Weapon = request.models.Weapon;


        var name = request.query.name;
        //var userId = request.query.userId;

        Weapon.create({

            name   : name,
            userId : userId

        }).then(function(weapon) {

            return reply({

                message : 'Weapon created',
                content : weapon

            }).code(201);

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeWeaponName : function(request, reply) {

        var Weapon = request.models.Weapon;


        var id   = request.params.id;
        var name = request.query.name;

        Weapon.findOne({

            where : {
                id : id
            }

        }).then(function(weapon) {

            if (!weapon) {

                return reply(
                    Boom.notFound('Weapon not found')
                );

            }


            weapon.change({

                name : name

            }).then(function(weapon) {

                return reply({

                    message : 'Name changed'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    deleteWeapon : function(request, reply) {

        var Weapon = request.models.Weapon;


        var id = request.params.id;

        Weapon.findOne({

            where : {
                id : id
            }

        }).then(function(weapon) {

            if (!weapon) {

                return reply(
                    Boom.notFound('Weapon not found')
                );

            }

            weapon.destroy().then(function(num) {

                return reply({

                    message : 'Weapon deleted'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    }

};