var Boom   = require('boom');


module.exports = {

    findWeapon : function(request, reply) {

        var GameWeapon = request.models.GameWeapon;
        var Weapon     = request.models.Weapon;


        var gameId   = request.params.gameId;
        var weaponId = request.params.weaponId;

        if (!weaponId) {

            // Return all weapons
            Weapon.findAll({

                include : [
                    {
                        model : GameWeapon,
                        where : {
                            game_id : gameId
                        }
                    }
                ]

            }).then(function(weapons) {

                return reply(weapons).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        } else {

            id = encodeURIComponent(request.params.id);

            // Return specific weapon
            Weapon.findOne({

                include : [{

                    model : GameWeapon,

                    where : {
                        game_id   : gameId,
                        weapon_id : weaponId
                    }

                }]

            }).then(function(weapon) {

                if (!weapon) {

                    return reply(
                        Boom.notFound('Weapon not found')
                    );

                }

                return reply(weapon).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }

    },

    addWeapon : function(request, reply) {

        var GameWeapon = request.models.GameWeapon;


        var gameId   = request.params.gameId;
        var weaponId = request.params.weaponId;

        GameWeapon.create({

            gameId   : gameId,
            weaponId : weaponId

        }).then(function(weapon) {

            return reply({

                message : 'Weapon added',
                content : weapon

            }).code(201);

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    removeWeapon : function(request, reply) {

        var GameWeapon = request.models.GameWeapon;


        var gameId   = request.params.gameId;
        var weaponId = request.params.weaponId;

        GameWeapon.findOne({

            where : {
                gameId   : gameId,
                weaponId : weaponId
            }

        }).then(function(weapon) {

            if (!weapon) {

                return reply(
                    Boom.notFound('Weapon not found')
                );

            }

            weapon.destroy().then(function(num) {

                return reply({

                    message : 'Weapon deleted'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    }

};