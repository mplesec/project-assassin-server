var Boom   = require('boom');
var jwt    = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Auth   = require('./helpers/auth');


module.exports = {

    findUser : function(request, reply) {

        var User = request.models.User;


        var id = request.params.id;

        if (!id) {

            // Return all users
            User.findAll({

                // Only select the following
                attributes : ['email', 'name', 'surname', 'location']

            }).then(function(users) {

                // return is used to end the function execution
                return reply(users).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        } else {

            id = encodeURIComponent(request.params.id);

            // Return specific user
            User.findOne({

                where : {
                    id : id
                },

                attributes : ['email', 'name', 'surname', 'location']

            }).then(function(user) {

                if (!user) {

                    return reply(
                        Boom.notFound('User not found')
                    );

                }

                return reply(user).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }

    },

    loginUser : function(request, reply) {

        var User = request.models.User;


        var email    = request.query.email;
        var password = request.query.password;

        User.findOne({

            where : {
                email : email
            }

        }).then(function(user) {

            if (!user) {

                return reply(
                    Boom.notFound('Email or password incorrect')
                );

            }

            // Check if passwords match
            bcrypt.compare(password, user.password, function(err, res) {

                if (!res) {

                    return reply(
                        Boom.notFound('Email or password incorrect')
                    );

                }


                var token = Auth.getToken(
                    request,
                    {
                        id      : user.id,
                        email   : user.email
                    }
                );

                return reply({
                    token : token,
                    user : {
                        email    : user.email,
                        name     : user.name,
                        surname  : user.surname,
                        location : user.location
                    }
                });

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    // TODO NOT working with hashing, change to changePassword
    getUserPassword : function(request, reply) {

        var User = request.models.User;


        var email = request.query.email;

        User.findOne({

            where : {
                email : email
            }

        }).then(function(user) {

            if (!user) {

                return reply(
                    Boom.notFound('User not found')
                );

            }


            return reply(user.password).code(200);

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    createUser : function(request, reply) {

        var User = request.models.User;


        var email           = request.query.email;
        var password        = request.query.password;
        var passwordConfirm = request.query.passwordConfirm;
        var name            = request.query.name;
        var surname         = request.query.surname;
        var location        = request.query.location;

        if (password !== passwordConfirm) {

            return reply(
                Boom.conflict('Passwords do not match')
            );

        }

        // Check if user with this email already exists
        User.findOne({

            where : {
                email : email
            }

        }).then(function(user) {

            if (user) {

                return reply(
                    Boom.conflict('Email already in use')
                );

            }


            // Salt is automatically added in front of the hash

            var salt = bcrypt.genSaltSync( Config.get('auth.salt_sync_rounds') );
            var hash = bcrypt.hashSync(passwordConfirm, salt);

            User.create({

                email    : email,
                password : hash,
                name     : name,
                surname  : surname,
                location : location

            }).then(function(user) {

                return reply({

                    message : 'User created',
                    content : {

                        email    : user.email,
                        name     : user.name,
                        surname  : user.surname,
                        location : user.location

                    }

                }).code(201);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeUserPassword : function(request, reply) {

        var User = request.models.User;


        var id                 = request.query.id;
        var passwordCurrent    = request.query.passwordCurrent;
        var passwordNew        = request.query.passwordNew;
        var passwordNewConfirm = request.query.passwordNewConfirm;

        User.findOne({

            where : {
                id : id
            }

        }).then(function(user) {

            var password = user.password;

            if (passwordCurrent !== password) {

                return reply(
                    Boom.notFound('Current password does not match set password')
                );

            }


            user.change({

                password : passwordNewConfirm

            }).then(function(user) {

                return reply({

                    message : 'Password changed'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeUserLocation : function(request, reply) {

        var User = request.models.User;


        var id       = request.query.id;
        var location = request.query.location;

        User.findOne({

            where : {
                id : id
            }

        }).then(function(user) {

            if (!user) {

                return reply(
                    Boom.notFound('User not found')
                );

            }


            user.change({

                location : location

            }).then(function(user) {

                return reply({

                    message : 'Location changed'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    deleteUser : function(request, reply) {

        var User = request.models.User;


        var id    = request.params.id;
        var email = request.query.email;

        User.findOne({

            where : {
                id : id
            }

        }).then(function(user) {

            if (!user) {

                return reply(
                    Boom.notFound('User not found')
                );

            }

            if (user.email !== email) {

                return reply(
                    Boom.notFound('User not found')
                );

            }

            user.destroy().then(function(num) {

                return reply({

                    message : 'User deleted'

                }).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            // Same as request.server.log()
            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    }


    /*// @param user  User based on whos location we provide a list of users
    // @param num   Maximum number of users returned (can b less, or even zero)
    getUserBasedUsersOnLocation : function(user, num) {

        var defaultNum = 10;
        num = (num !== 'undefined') ? num : defaultNum;

        // TODO Find nearest based on user's location
        // TODO While loop, until x km's

    }*/

};