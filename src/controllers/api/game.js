var Boom = require('boom');


var helpers = {

    // Creates pairs out of an array of user ids
    // @param users  Array of user ids (i.e. [1, 2, 3, 4])
    generateUserPairs : function(users) {

        return new Promise(function(resolve, reject) {

            var numUsers = users.length;

            // Check if there are at least two users
            if (numUsers < 2) {
                reject(Error("Must have at least 2 users"));
            }


            // Generate pairs

            var pairs = {};

            // Choose first assassin
            var first = assassin = users[ Math.floor(Math.random() * users.length) ];
            var target;

            while (users.length > 0) {

                // Choose assassin
                if (target)
                    assassin = target;

                users.splice(users.indexOf(assassin), 1);

                // Choose target
                target = users[ Math.floor(Math.random() * users.length) ];
                if (users.length === 0)
                    target = first;

                pairs.push({
                    game_user_assassin_id : assassin,
                    game_user_target_id   : target
                });

            }

            // Last pair
            pairs.push({
                game_user_assassin_id : assassin,
                game_user_target_id   : target
            });


            // Check if correct number of pairs has been created
            if (pairs.length === numUsers) {

                resolve(pairs);

            } else {

                reject(Error("Something went wrong"));

            }

        });

    }

};


module.exports = {

    findGame : function(request, reply) {

        var Game = request.models.Game;


        var id = request.params.id;

        if (!id) {

            // Return all games
            Game.findAll().then(function(games) {

                return reply(games).code(200);

            }).catch(function(err) {

                // Same as request.server.log()
                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        } else {

            id = encodeURIComponent(request.params.id);

            // Return specific game
            Game.findOne({

                where : {
                    id : id
                }

            }).then(function(game) {

                if (!game) {

                    return reply(
                        Boom.notFound('Game not found')
                    );

                }

                return reply(game).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }

    },

    createGame : function(request, reply) {

        var Game     = request.models.Game;
        var GameUser = request.models.GameUser;


        var name                       = request.query.name;
        var playersMin                 = request.query.playersMin;
        var playersMax                 = request.query.playersMax;
        var timeLimitTargetElimination = request.query.timeLimitTargetElimination;
        var startedAt                  = request.query.startedAt;
        var location                   = request.query.location;
        var userId                     = request.auth.credentials.user.id;

        // TODO (properly) set time for startedAt properly

        // TODO check if, but not here, must be in model  Together, name and location must be different
        Game.find({

            where : {
                name     : name,
                location : location
            }

        }).then(function(game) {

            if (game) {

                return reply(
                    Boom.conflict('Game with this name at this location already exists')
                );

            }


            Game.create({

                name                               : name,
                playersMin                         : playersMin,
                playersMax                         : playersMax,
                time_limit_target_elimination_time : timeLimitTargetElimination,
                location                           : location,
                user_id                            : userId

            }).then(function(game) {

                return reply({

                    message : 'Game created',
                    content : game

                }).code(201);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeGameName : function(request, reply) {

        var Game = request.models.Game;


        var id   = request.params.id;
        var name = request.query.name;

        Game.findOne({

            where : {
                id : id
            }

        }).then(function(game) {

            if (!game) {

                return reply(
                    Boom.notFound('Game not found')
                );

            }


            game.change({

                name : name

            }).then(function(game) {

                return reply({

                    message : 'Name changed'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeGameType : function(request, reply) {

        var Game = request.models.Game;


        var id   = request.params.id;
        var type = request.query.type;

        Game.findOne({

            where : {
                id : id
            }

        }).then(function(game) {

            if (!game) {

                return reply(
                    Boom.notFound('Game not found')
                );

            }


            game.change({

                type : type

            }).then(function(game) {

                return reply({

                    message : 'Type changed'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    changeGameLocation : function(request, reply) {

        var Game = request.models.Game;


        var id       = request.params.id;
        var location = request.query.location;

        Game.findOne({

            where : {
                id : id
            }

        }).then(function(game) {

            if (!game) {

                return reply(
                    Boom.notFound('Game not found')
                );

            }


            game.change({

                location : location

            }).then(function(game) {

                return reply({

                    message : 'Location changed'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    deleteGame : function(request, reply) {

        var Game = request.models.Game;


        var id = request.params.id;

        Game.findOne({

            where : {
                id : id
            }

        }).then(function(game) {

            if (!game) {

                return reply(
                    Boom.notFound('Game not found')
                );

            }

            game.destroy().then(function(num) {

                return reply({

                    message : 'Game deleted'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },


    joinGame : function(request, reply) {

        var Game     = request.models.Game;
        var GameUser = request.models.GameUser;


        var gameId = request.params.id;
        var userId = request.auth.credentials.user.id;

        // Check if game is full

        Game.findOne({

            where : {
                id : gameId
            }

        }).then(function(game) {

            var maxPlayers = game.players_max;


            GameUser.findAll({

                where : {
                    game_id : gameId
                }

            }).then(function(gameUsers) {

                if (gameUsers.length >= maxPlayers) {

                    return reply({

                        message : 'Could not join game. Game is full.'

                    }).code(200);

                }

                GameUser.findOne({

                    user_id : userId,
                    game_id : gameId

                }).then(function(gameUser) {

                    if (gameUser) {

                        return reply(
                            Boom.conflict('Already joined this game')
                        );

                    }


                    GameUser.create({

                        user_id : userId,
                        game_id : gameId

                    }).then(function(gameUser) {

                        return reply({

                            message : 'Joined game'

                        }).code(200);

                    }).catch(function(err) {

                        request.log(['error', 'database', 'read'], err);

                        return reply(
                            Boom.badImplementation('Database error')
                        );

                    });

                }).catch(function(err) {

                    request.log(['error', 'database', 'read'], err);

                    return reply(
                        Boom.badImplementation('Database error')
                    );

                });

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    leaveGame : function(request, reply) {

        var Game     = request.models.Game;
        var GameUser = request.models.GameUser;


        var gameId = request.params.id;
        var userId = request.auth.credentials.user.id;

        GameUser.findOne({

            where : {
                game_id : gameId,
                user_id : userId
            }

        }).then(function(gameUser) {

            gameUser.destroy().then(function(num) {

                return reply({

                    message : 'Left game'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    startGame : function(request, reply) {

        var Game     = request.models.Game;
        var GameUser = request.models.GameUser;
        var GamePair = request.models.GamePair;


        // Check if user has permission (has created this game)
        var gameId = request.params.id;
        var userId = request.auth.credentials.user.id;

        Game.findOne({

            where : {
                id      : gameId,
                user_id : userId
            }

        }).then(function(game) {

            if (!game) {

                return reply(
                    Boom.forbidden('You do not have permission to start this game')
                );

            }


            server.log("STARTING GAME");
            // Start the game
            game.update({

                // TODO figure this out, how to get NOW() for date
                //started_at : request.server.plugins['hapi-sequelized'].fn('NOW')
                started_at : request.server.plugins['hapi-sequelized'].db.Sequelize.NOW

            }).then(function(game) {

                // Pair up the users

                GameUser.findAll({

                    attributes : [ 'user_id' ],

                    where : {
                        game_id : gameId
                    },

                    raw : true

                }).then(function(gameUsers) {

                    server.log("game users in game [id : " + gameId + "]:");
                    server.log(gameUsers);

                    var gameUsersFormatted = [];
                    gameUsers.forEach(function(gameUser) {

                        gameUsersFormatted.push( gameUser.user_id );

                    });

                    helpers.generateUserPairs(gameUsersFormatted).then(function(pairs) {

                        //server.log("pairs generated:");
                        //server.log(pairs);

                        // Save pairs
                        GamePair.bulkCreate(pairs).then(function(num) {

                            return reply({

                                message : 'Game started',
                                content : {
                                    pairs : pairs
                                }

                            }).code(200);

                        });

                    });

                });

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        });

    }

};