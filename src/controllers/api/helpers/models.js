/* Abstracts code to access hapi models */

var internals = {};

module.exports = internals;

internals.getModel = function(server, name) {

    return server.plugins['hapi-sequelized'].db.sequelize.models[name];

};