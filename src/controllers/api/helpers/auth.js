var jwt = require('jsonwebtoken');

var Models = require('./models');

var config = require('config');


/*
 * After the client receives a token, the client may use it to access restricted API services.
 * Client requests must add a token to the HTTP header as 'Authorization', and must be prefixed
 * with 'Bearer ', i.e. Authorization: Bearer tokenvalue
 */

var internals = {};

module.exports = internals;

internals.tokenSecret     = config.get('auth.token.secret');
internals.tokenExpiration = config.get('auth.token.expiration');

// Used in building web request. Must be added to the headers as 'authorization', and prefixed with 'Bearer'
// @param content  Information included in the token
internals.getToken = function(request, content) {

    var token = jwt.sign({

            // User agent
            agent : request.headers['User-Agent'],

            // Expiration time
            exp : Math.floor(new Date().getTime() / 1000) + this.tokenExpiration, // In seconds

            content

        },

        this.tokenSecret

    );


    return token;

};

// Expiration is checked by default in the jwt module
// @param decoded  Client-side information
internals.validateToken = function(decoded, request, callback) {

    // Check if same browser
    if (request.headers['User-Agent'] !== decoded.agent) {

        return callback(null, false);

    }

    var User = Models.getModel(request.server, 'User');

    User.findOne({

        where : {
            email : decoded.content.email
        }

    }).then(function(user) {

        if (!user) {

            return callback(null, false);

        }

        return callback(null, true, {

            user : {

                // Client-side information needed further on in routing (i.e. request.auth.credentials.user.id)
                id      : user.id,
                email   : user.email,
                name    : user.name,
                surname : user.surname

            }

        });

    });

};