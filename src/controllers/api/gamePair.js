var Boom   = require('boom');


var helpers = {

    isPair : function(GamePair, assassinId, targetId) {

        GamePair.findOne({

            where : {
                game_user_assassin_id : targetId,
                game_user_target_id   : assassinId
            }

        }).then(function(pair) {

            if (!pair)
                return false;

            return pair;

        }).catch(function(err) {

            server.log(['error', 'database', 'read'], err);

            return false;

        });

    },

    findPairByAssassinId : function(server, assassinId) {

        var GamePair = request.models.GamePair;


        GamePair.findOne({

            where : {
                game_user_assassin_id : assassinId
            }

        }).then(function(pair) {

            if (!pair)
                return false;

            return pair;

        });

    },

    findPairByTargetId : function(server, targetId) {

        var GamePair = request.models.GamePair;


        GamePair.findOne({

            where : {
                id : targetId
            }

        }).then(function(pair) {

            if (!pair)
                return false;

            return pair;

        });

    },

    addPair : function(server, assassinId, targetId) {

        var GamePair = request.models.GamePair;


        GamePair.create({

            game_user_assassin_id : targetId,
            game_user_target_id   : assassinId

        }).then(function(pair) {

            return true;

        }).catch(function(err) {

            server.log(['error', 'database', 'read'], err);

            return false;

        });

    },

    removePair : function(server, assassinId, targetId) {

        var GamePair = request.models.GamePair;


        GameWeapon.findOne({

            where : {
                game_user_assassin_id : targetId,
                game_user_target_id   : assassinId
            }

        }).then(function(pair) {

            if (!pair)
                return false;

            pair.destroy().then(function(num) {

                return true;

            }).catch(function(err) {

                server.log(['error', 'database', 'read'], err);

                return false;

            });

        }).catch(function(err) {

            server.log(['error', 'database', 'read'], err);

            return false;

        });

    },

    // Removes the target and adds new pairs
    // @param claimId  The assassination claim id, used to id the assassin-target pair in a game
    assassinateTarget : function(models, claimId) {

        var GamePair = request.models.GamePair;


        var assassinId = request.params.assassinId;
        var targetId   = request.params.targetId;

        // Find pair with assassin and target
        GamePair.findOne({

            where : {
                game_user_assassin_id : assassinId,
                game_user_target_id   : targetId
            }

        }).then(function(pair) {

            if (!pair) {

                return reply(
                    Boom.notFound('Pair not found')
                );

            }

            // Find pair where target is the assassin of another user
            GamePair.findOne({

                where : {
                    game_user_assassin_id : target_id
                }

            }).then(function(pairTargetIsAssassin) {

                if (!pairTargetIsAssassin) {

                    return reply(
                        Boom.notFound('Pair not found')
                    );

                }


                // Get the ids for the new pair
                var pairAssassinId = pair.game_user_assassin_id;
                var pairTargetId   = pairTargetIsAssassin.game_user_target_id;

                // Remove the current pairs
                this.removePair(
                    pair.game_user_assassin_id,
                    pair.game_user_target_id
                );
                this.removePair(
                    pairTargetIsAssassin.game_user_assassin_id,
                    pairTargetIsAssassin.game_user_target_id
                );

                // Create a new pair
                this.addPair(
                    pairAssassinId,
                    pairTargetId
                );

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    /* After receiving a kill message from an assassin, the server sends a message to the target
     * seeking its confirmation of the kill
     */
    sendConfirmation : function(server, targetId) {

        // TODO push notifs for each platform individualy

    }

};

module.exports = {

    findPair : function(request, reply) {

        var GamePair = request.models.GamePair;


        var assassinId = request.params.assassinId;
        var targetId   = request.params.targetId;

        GamePair.findOne({

            where : {
                game_user_assassin_id : targetId,
                game_user_target_id   : assassinId
            }

        }).then(function(pair) {

            if (!pair) {

                return reply(
                    Boom.notFound('Pair not found')
                );

            }

            return reply(pair).code(200);

        }).catch(function(err) {

            server.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    createAssassinationClaim : function(request, reply) {

        var GamePairClaims = request.models.GamePairClaims;
        var GamePair       = request.models.GamePair;


        var assassinId = request.auth.credentials.user.id;
        var gameId     = request.params.gameId;

        GamePair.findOne({

            where : {
                game_user_assassin_id : assassinId
            }

        }).then(function(gamePair) {

            if (!gamePair) {

                return reply(
                    Boom.notFound('Pair not found')
                );

            }

            GamePairClaims.create({

                game_pair_id : gamePair.id

            }).then(function(claim) {

                return reply({

                    message : 'Assassination claim created'

                }).code(201);

            }).catch(function(err) {

                server.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            server.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    confirmAssassination : function(request, reply) {

        var GamePairClaims = request.models.GamePairClaims;
        var GamePair       = request.models.GamePair;


        // TODO assassin confirm to (other method)

        var targetId     = request.auth.credentials.user.id;
        var claimId      = request.params.claimId;
        var confirmation = request.params.confirmation;

        // Confirm the assassination
        GamePairClaims.findOne({

            where : {
                id : claimId
            },

            include : [
                GamePair
            ]

        }).then(function(gamePair) {

            if (!gamePair) {

                return reply(
                    Boom.notFound('Assassination claim was nou found')
                );

            }


            // Check that user has permission to confirm the assassination
            var gamePairTargetId = gamePair.game_user_target_id;

            if (gamePairTargetId !== targetId) {

                return reply(
                    Boom.forbidden(
                        'You are not allowed to confirm ' +
                        'or deny this assassination claim'
                    )
                );

            }


            // TODO destroy, and create new pairs via kill target
            //gamePair.

        });

    }

};