var Boom   = require('boom');


module.exports = {

    findGame : function(request, reply) {

        var GameUser = request.models.GameUser;


        var gameId = request.params.gameId;
        var userId = request.auth.credentials.user.id;

        GameUser.find({

            gameId : gameId,
            userId : userId

        }).then(function(gameUser) {

            if (!gameUser) {

                return reply(
                    Boom.notFound('User does not belong to this game')
                );

            }

            return reply(gameUser).code(200);

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    joinGame : function(request, reply) {

        var GameUser = request.models.GameUser;


        var gameId = request.params.gameId;
        var userId = request.auth.credentials.user.id;

        GameUser.create({

            gameId : gameId,
            userId : userId

        }).then(function(gameUser) {

            return reply({

                message : 'User added to game',
                content : gameUser

            }).code(201);

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    },

    leaveGame : function(request, reply) {

        var GameUser = request.models.GameUser;


        var gameId = request.params.gameId;
        var userId = request.auth.credentials.user.id;

        GameUser.findOne({

            where : {
                gameId : gameId,
                userId : userId
            }

        }).then(function(gameUser) {

            if (!gameUser) {

                return reply(
                    Boom.notFound('User does not belong to this game')
                );

            }

            gameUser.destroy().then(function(num) {

                return reply({

                    message : 'User was removed from this game'

                }).code(200);

            }).catch(function(err) {

                request.log(['error', 'database', 'read'], err);

                return reply(
                    Boom.badImplementation('Database error')
                );

            });

        }).catch(function(err) {

            request.log(['error', 'database', 'read'], err);

            return reply(
                Boom.badImplementation('Database error')
            );

        });

    }

};