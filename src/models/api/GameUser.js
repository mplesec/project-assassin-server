'use strict';


// Association table which stores which users are part of which game(s)
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('GameUser', {}, {

        tableName : 'game_user',

        hooks: {

            // Simulates a composite primary key (doesn't allow INSERT if already exists)
            beforeCreate: function(gameUser, options) {

                return new Promise(function(resolve, reject) {

                    sequelize.model('GameUser').findOne({

                        where : {
                            user_id : gameUser.user_id,
                            game_id : gameUser.game_id
                        }

                    }).then(function(gameUser) {

                        if (gameUser) {
                            reject('Already exists');
                        }
                        else {
                            resolve('Created');
                        }

                    });

                });

            }

        },

        // Must be present for Sequelize queries to work (even if foreign keys in definition are present)
        classMethods : {

            associate : function(models) {

                var GameUser = models.GameUser;
                var Game     = models.Game;
                var User     = models.User;


                User.hasMany(GameUser, {

                    foreignKey : 'user_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });
                GameUser.belongsTo(User, {

                    foreignKey : 'user_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });

                Game.hasMany(GameUser, {

                    foreignKey : 'game_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });
                GameUser.belongsTo(Game, {

                    foreignKey : 'game_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });

            }

        }

    });

};