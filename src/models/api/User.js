'use strict';


// Stores users
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('User', {

        email : {
            type      : DataTypes.STRING(256),
            allowNull : false,
            validate  : {
                isEmail : true
            }
        },

        password : {
            type      : DataTypes.STRING(128),
            allowNull : false
        },

        name : {
            type      : DataTypes.STRING(128),
            allowNull : false,
            validate  : {
                isAlpha : true
            }
        },

        surname : {
            type      : DataTypes.STRING(128),
            allowNull : false,
            validate  : {
                isAlpha : true
            }
        },

        location : {
            type      : DataTypes.STRING(1024),
            allowNull : false
        }

    });

};