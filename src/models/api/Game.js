'use strict';


// Stores games
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Game', {

        name : {
            type      : DataTypes.STRING(128),
            allowNull : false,
            validate  : {
                is : /^[a-zA-Z0-9 ]+$/
            }
        },

        // Public or private
        type : {
            type         : DataTypes.BOOLEAN,
            allowNull    : false,
            defaultValue : true
        },

        // Silent mode (no notifications to players when a player is killed)
        silent : {
            type         : DataTypes.BOOLEAN,
            allowNull    : false,
            defaultValue : false
        },

        playersMin : {
            field        : 'players_min',
            type         : DataTypes.INTEGER,
            allowNull    : false,
            defaultValue : 2
        },

        playersMax : {
            field        : 'players_max',
            type         : DataTypes.INTEGER,
            allowNull    : false,
            defaultValue : 64
        },

        timeLimitTargetElimination : {
            field        : 'time_limit_target_elimination',
            type         : DataTypes.TIME,
            allowNull    : false,
            defaultValue : '24:00:00' // 'HHH:MM:SS'
        },

        // Defined here to note its importance to the model
        createdAt : {
            field        : 'created_at',
            type         : DataTypes.DATE,
            allowNull    : false,
            defaultValue : DataTypes.NOW
        },

        // When the game starts (user can set when the game will start or start it manually)
        startedAt : {
            field        : 'started_at',
            type         : DataTypes.DATE,
            allowNull    : true // null means game has not started yet
        },

        location : {
            type      : DataTypes.STRING,
            allowNull : false
        }

    }, {

        // Don't add the default timestamp attributes (updatedAt, createdAt)
        timestamps : false,
        // Set attribute for createdAt
        createdAt  : 'createdAt',

        classMethods : {

            associate : function(models) {

                var Game = models.Game;
                var User = models.User;


                User.hasMany(Game, {

                    foreignKey : 'user_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });
                Game.belongsTo(User, {

                    foreignKey : 'user_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });

            }

        },

        hooks : {

            // Add game creator to gameUser
            afterCreate : function(game, options) {

                var GameUser = sequelize.models.GameUser;

                return new Promise(function(resolve, reject) {

                    GameUser.create({

                        user_id : game.user_id,
                        game_id : game.id

                    }).then(function(gameUser) {

                        if (gameUser) {

                            resolve('Added to gameUser');

                        } else {

                            reject('Could not add to gameUser');

                        }

                    });

                });

            }

        }

    });

};