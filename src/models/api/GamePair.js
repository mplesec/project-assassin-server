'use strict';


// Association table which stores which users are part of which game(s)
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('GamePair', {}, {

        tableName : 'game_pair',

        classMethods : {

            associate : function(models) {

                var GamePair = models.GamePair;
                var GameUser = models.GameUser;


                // This is enough for these associations to work properly (https://github.com/sequelize/sequelize/issues/3678)

                GamePair.belongsTo(GameUser, {

                    as         : 'game_user_assassin',  // _id is auto added at the end

                    foreignKey : 'game_user_assassin_id_fk',

                    onDelete   : 'CASCADE',
                    onUpdate   : 'CASCADE'

                });

                GamePair.belongsTo(GameUser, {

                    as         : 'game_user_target',

                    foreignKey : 'game_user_target_id_fk',

                    onDelete   : 'CASCADE',
                    onUpdate   : 'CASCADE'

                });

            }

        },

        hooks: {

            // Acts as a composite primary key (doesn't allow if already exists)
            beforeCreate : function(gamePair, options) {

                return new Promise(function(resolve, reject) {

                    sequelize.model('GamePair').findOne({

                        where : {
                            game_user_assassin_id : gamePair.game_user_assassin_id,
                            game_user_target_id   : gamePair.game_user_target_id
                        }

                    }).then(function(gamePair) {

                        if (gamePair) {

                            reject('Already exists');

                        } else {

                            resolve('Created');

                        }

                    });

                });

            }

        }

    });

};