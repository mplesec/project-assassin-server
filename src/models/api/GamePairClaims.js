'use strict';


// Stores the assassination claims made by a user
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('GamePairClaims', {}, {

        tableName : 'game_pair_claims',

        classMethods : {

            associate : function(models) {

                var GamePairClaims = models.GamePairClaims;
                var GamePair       = models.GamePair;


                // This is enough for these associations to work properly (https://github.com/sequelize/sequelize/issues/3678)

                GamePairClaims.belongsTo(GamePair, {

                    as         : 'game_pair',
                    foreignKey : 'game_pair_id_fk',

                    onDelete   : 'CASCADE',
                    onUpdate   : 'CASCADE'

                });

            }

        }

    });

};