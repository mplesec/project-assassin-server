'use strict';


// Stores weapons
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('Weapon', {

        name : {
            type      : DataTypes.STRING(128),
            allowNull : false,
            validate  : {
                is : /^[a-zA-Z0-9 ]+$/
            }
        }

    }, {

        timestamps : false,

    });

};