'use strict';


// Stores which weapons a game can have
module.exports = function(sequelize, DataTypes) {

    return sequelize.define('GameWeapon', {}, {

        tableName : 'game_weapon',

        hooks: {

            // Simulates a composite primary key (doesn't allow INSERT if already exists)
            beforeCreate: function(gameWeapon, options) {

                return new Promise(function(resolve, reject) {

                    sequelize.model('GameWeapon').findOne({

                        where : {
                            game_id   : gameWeapon.game_id,
                            weapon_id : gameWeapon.weapon_id
                        }

                    }).then(function(gameWeapon) {

                        if (gameWeapon) {
                            reject('Already exists');
                        }
                        else {
                            resolve('Created');
                        }

                    });

                });

            }

        },

        classMethods : {

            associate : function(models) {

                var GameWeapon = models.GameWeapon;
                var Game       = models.Game;
                var Weapon     = models.Weapon;


                Game.hasMany(GameWeapon, {

                    foreignKey : 'game_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });
                GameWeapon.belongsTo(Game, {

                    foreignKey : 'game_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });

                Weapon.hasMany(GameWeapon, {

                    foreignKey : 'weapon_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });
                GameWeapon.belongsTo(Weapon, {

                    foreignKey : 'weapon_id',
                    targetKey  : 'id',

                    onDelete : 'CASCADE',
                    onUpdate : 'CASCADE'

                });

            }

        }

    });

};