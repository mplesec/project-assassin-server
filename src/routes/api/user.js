var Joi = require('Joi');

var controller = require('../../controllers/api/user');
// ?TODO create an authentication controller for login and signup


module.exports = {

    init : function init(server, path) {

        server.route([
            {
                method  : 'POST',
                path    : path + '/signup',
                handler : controller.createUser,
                config  : {
                    auth : false,
                    validate : {
                        query : {
                            /*email           : {
                                args : Joi.string().email().required(),
                                msg  : 'Email cannot be null'
                            },*/
                            email           : Joi.string().email().required(),
                            password        : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/).required(),
                            passwordConfirm : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/).required(),
                            name            : Joi.string().alphanum().min(2).required(),
                            surname         : Joi.string().alphanum().min(2).required(),
                            location        : Joi.string().required()
                        }
                    }
                }
            },
            {
                method  : 'GET',
                path    : path + '/login',
                handler : controller.loginUser,
                config  : {
                    auth : false,
                    validate : {
                        query : {
                            email    : Joi.string().email().required(),
                            password : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/).required()
                        }
                    }
                }
            },
            {
                method  : 'GET',
                path    : path + '/user/{id?}',
                handler : controller.findUser,
                config  : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive()
                        }
                    }
                }
            },
            {
                method  : 'GET',
                path    : path + '/user/auth/recover-password',
                handler : controller.getUserPassword,
                config  : {
                    validate : {
                        query : {
                            email : Joi.string().email().required()
                        }
                    }
                }
            },
            {
                method  : 'PUT',
                path    : path + '/user/password',
                handler : controller.changeUserPassword,
                config : {
                    validate : {
                        query : {
                            // TODO get via token in pre
                            id                 : Joi.number().integer().positive().required(),
                            passwordCurrent    : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/),
                            passwordNew        : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/),
                            passwordNewConfirm : Joi.string().regex(/^[a-zA-Z0-9]{8,128}$/)
                        }
                    }
                }
            },
            {
                method  : 'PUT',
                path    : path + '/user/{id}/location',
                handler : controller.changeUserLocation,
                config : {
                    validate : {
                        query : {
                            // TODO get via token in pre
                            id       : Joi.number().integer().positive().required(),
                            location : Joi.string().required()
                        }
                    }
                }
            },
            {
                method  : 'DELETE',
                path    : path + '/user',
                handler : controller.deleteUser,
                config : {
                    validate : {
                        query : {
                            // TODO get via token in pre
                            id    : Joi.number().integer().positive().required(),
                            // User must enter to confirm deletion
                            email : Joi.string().email().required()
                        }
                    }
                }
            }
        ]);

    }

};