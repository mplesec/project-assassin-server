var Joi = require('Joi');

var controller = require('../../controllers/api/gamePair');


module.exports = {

    init : function init(server, path) {

        server.route([
            // An assassin wants to know who its target is
            {
                method  : 'GET',
                path    : path + '/game/{gameId}/target',
                handler : controller.findPair,
                config : {
                    validate :  {
                        params :  {
                            gameId : Joi.number().integer().positive().required()
                        }
                    }
                }
            },
            // Assassin has assassinated its target
            {
                method  : 'POST',
                path    : path + '/game/{gameId}/target/assassinate',
                handler : controller.createAssassinationClaim,
                config : {
                    validate :  {
                        params :  {
                            gameId : Joi.number().integer().positive().required()
                        }
                    }
                }
            },
            // Target confirms or denies the assassination
            {
                method  : 'POST',
                path    : path + '/game/claim/{claimId}/confirmation/{confirmation?}',
                handler : controller.confirmAssassination,
                config  : {
                    validate : {
                        params :  {
                            claimId      : Joi.number().integer().positive().required(),
                            confirmation : Joi.boolean().default(true)
                        }
                    }
                }
            }
        ]);

    }

};