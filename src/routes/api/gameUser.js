var Joi = require('Joi');

var controller = require('../../controllers/api/gameUser');


module.exports = {

    init : function init(server, path) {

        server.route([
            {
                method  : 'GET',
                path    : path + '/user/game/{gameId?}',
                handler : controller.findGame,
                config : {
                    validate :  {
                        params :  {
                            gameId : Joi.number().integer().positive()
                        }
                    }
                }
            },
            {
                method  : 'POST',
                path    : path + '/user/game/{gameId}',
                handler : controller.joinGame,
                config  : {
                    validate : {
                        params :  {
                            gameId : Joi.number().integer().positive()
                        }
                    }
                }
            },
            {
                method  : 'DELETE',
                path    : path + '/user/game/{gameId}',
                handler : controller.leaveGame,
                config  : {
                    validate : {
                        params : {
                            gameId : Joi.number().integer().positive()
                        }
                    }
                }
            }
        ]);

    }

};