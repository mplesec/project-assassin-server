var Joi = require('Joi');

var controller = require('../../controllers/api/game');


module.exports = {

    init : function init(server, path) {

        server.route([
            {
                method  : 'GET',
                path    : path + '/game/{id?}',
                handler : controller.findGame,
                config : {
                    validate :  {
                        params :  {
                            id : Joi.number().integer().positive()
                        }
                    }
                }
            },
            {
                method  : 'POST',
                path    : path + '/game',
                handler : controller.createGame,
                config  : {
                    validate : {
                        query : {
                            name                       : Joi.string().alphanum().required(),
                            type                       : Joi.boolean().default(true),
                            silent                     : Joi.boolean().default(false),
                            playersMin                 : Joi.number().integer().min(2).default(2),
                            playersMax                 : Joi.number().integer().max(64).default(64),
                            timeLimitTargetElimination : Joi.date().format('HHH:mm:ss').default('24:00:00'),
                            startedAt                  : Joi.date().format('YYYY:MM:DD:HHH:mm'),
                            location                   : Joi.string().alphanum().required()
                        }
                    }
                }
            },
            {
                method  : 'PUT',
                path    : path + '/game/{id}/name',
                handler : controller.changeGameName,
                config  : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        },
                        query : {
                            name   : Joi.string().alphanum().required(),
                            // TODO get this in pre via token
                            userId : Joi.number().integer().required()
                        }
                    }
                }
            },
            {
                method  : 'PUT',
                path    : path + '/game/{id}/type',
                handler : controller.changeGameType,
                config  : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        },
                        query : {
                            type   : Joi.boolean().required(),
                            // TODO get this in pre via token
                            userId : Joi.number().integer().required()
                        }
                    }
                }
            },
            {
                method  : 'PUT',
                path    : path + '/game/{id}/location',
                handler : controller.changeGameLocation,
                config  : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        },
                        query : {
                            location : Joi.string().alphanum().required(),
                            // TODO get this in pre via token
                            userId   : Joi.number().integer().required()
                        }
                    }
                }
            },
            {
                method  : 'DELETE',
                path    : path + '/game/{id}',
                handler : controller.deleteGame,
                config  : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive()
                        }
                    }
                }
            },

            // Game lobby
            {
                method  : 'POST',
                path    : path + '/game/{id}/join',
                handler : controller.joinGame,
                config : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        }
                    }
                }
            },
            {
                method  : 'POST',
                path    : path + '/game/{id}/leave',
                handler : controller.leaveGame,
                config : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        }
                    }
                }
            },
            {
                method  : 'POST',
                path    : path + '/game/{id}/start',
                handler : controller.startGame,
                config : {
                    validate : {
                        params : {
                            id : Joi.number().integer().positive().required()
                        }
                    }
                }
            }
        ]);

    }

};