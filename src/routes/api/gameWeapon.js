var Joi = require('Joi');

var controller = require('../../controllers/api/gameWeapon');


module.exports = {

    init : function init(server, path) {

        server.route([
            {
                method  : 'GET',
                path    : path + '/game/{gameId}/weapon/{weaponId?}',
                handler : controller.findWeapon,
                config : {
                    validate :  {
                        params :  {
                            gameId   : Joi.number().integer().positive().required(),
                            weaponId : Joi.number().integer().positive()
                        }
                    }
                }
            },
            {
                method  : 'POST',
                path    : path + '/game/{gameId}/weapon/{weaponId}',
                handler : controller.addWeapon,
                config  : {
                    validate : {
                        params : {
                            gameId   : Joi.number().integer().positive().required(),
                            weaponId : Joi.number().integer().positive().required()
                        }
                    }
                }
            },
            {
                method  : 'DELETE',
                path    : path + '/game/{gameId}/weapon/{weaponId}',
                handler : controller.removeWeapon,
                config  : {
                    validate : {
                        params : {
                            gameId   : Joi.number().integer().positive().required(),
                            weaponId : Joi.number().integer().positive().required()
                        }
                    }
                }
            }
        ]);

    }

};