var controller = require('./../../controllers/api/weapon');


/*
 * Adding and changing weapons is currently unavailable to users
 */
module.exports = {

    init : function init(server, path) {

        server.route(
            {
                method  : 'GET',
                path    : path + '/weapon/{id?}',
                handler : controller.findWeapon
            }
        );

    }

};