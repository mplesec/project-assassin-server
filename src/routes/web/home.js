var Joi = require('Joi');


module.exports = {

    init : function init(server) {

        server.route(
            {
                method  : 'GET',
                path    : '/',
                handler : function(request, reply) {

                    return reply('Hello Assassin');

                },
                config : {
                    auth : false
                }
            }
        );

    }

};