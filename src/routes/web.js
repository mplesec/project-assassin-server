var homeRoutes = require('./web/home');


module.exports = {

    init : function init(server) {

        homeRoutes.init(server);

    }

}