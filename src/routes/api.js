var userRoutes       = require('./api/user');
var gameRoutes       = require('./api/game');
var gamePairRoutes   = require('./api/gamePair');
var gameUserRoutes   = require('./api/gameUser');
var gameWeaponRoutes = require('./api/gameWeapon');
var weaponRoutes     = require('./api/weapon');


module.exports = {

    init : function init(server, path) {

        userRoutes.init(server, path);
        gameRoutes.init(server, path);
        gamePairRoutes.init(server, path);
        gameUserRoutes.init(server, path);
        gameWeaponRoutes.init(server, path);
        weaponRoutes.init(server, path);

    }

}