var config = require('config');

var bcrypt = require('bcryptjs');


module.exports = {

    init : function(db) {

        var Models     = db.sequelize.models;

        var User       = Models['User'];
        var Game       = Models['Game'];
        var GamePair   = Models['GamePair'];
        var GameUser   = Models['GameUser'];
        var GameWeapon = Models['GameWeapon'];
        var Weapon     = Models['Weapon'];


        // Data to populate with

        // Password security
        var rounds = config.has('auth.salt_sync_rounds') ? config.get('auth.salt_sync_rounds') : 10; // 10 is default

        var users = [
            {
                email    : 'some.guy@gmail.com',
                password : bcrypt.hashSync( 'password1', bcrypt.genSaltSync(rounds) ),
                name     : 'Some',
                surname  : 'Guy',
                location : 'Los Angeles, California, USA'
            },
            {
                email    : 'jimmy.changa@gmail.com',
                password : bcrypt.hashSync( 'password2', bcrypt.genSaltSync(rounds) ),
                name     : 'Jimmy',
                surname  : 'Changa',
                location : 'Los Angeles, California, USA'
            },
            {
                email    : 'mike.dude@gmail.com',
                password : bcrypt.hashSync( 'password3', bcrypt.genSaltSync(rounds) ),
                name     : 'Mike',
                surname  : 'Dude',
                location : 'Miami, Florida, USA'
            },
            {
                email    : 'myron.gains@gmail.com',
                password : bcrypt.hashSync( 'password4', bcrypt.genSaltSync(rounds) ),
                name     : 'Myron',
                surname  : 'Gains',
                location : 'New York City, New York, USA'
            },
            {
                email    : 'george.dubya@gmail.com',
                password : bcrypt.hashSync( 'password5', bcrypt.genSaltSync(rounds) ),
                name     : 'George',
                surname  : 'Dubya',
                location : 'Miami, Florida, USA'
            },
            {
                email    : 'lawrence.pratt@gmail.com',
                password : bcrypt.hashSync( 'password6', bcrypt.genSaltSync(rounds) ),
                name     : 'Lawrence',
                surname  : 'Pratt',
                location : 'Los Angeles, California, USA'
            },
            {
                email    : 'michael.stein@gmail.com',
                password : bcrypt.hashSync( 'password7', bcrypt.genSaltSync(rounds) ),
                name     : 'Michael',
                surname  : 'Stein',
                location : 'Los Angeles, California, USA'
            },
            {
                email    : 'jimmy.hoffa@gmail.com',
                password : bcrypt.hashSync( 'password8', bcrypt.genSaltSync(rounds) ),
                name     : 'Jimmy',
                surname  : 'Hoffa',
                location : 'New York City, New York, USA'
            }
        ];

        var weapons = [
            { name : 'Sticky note' },
            { name : 'Elastic'     },
            { name : 'Stick'       },
            { name : 'Water gun'   }
        ];


        // Populate

        User.bulkCreate(users).then(function() {

            User.findAll().then(function(users) {

                Weapon.bulkCreate(weapons).then(function() {

                    Weapon.findAll().then(function(weapons) {

                        Game.bulkCreate(

                            [
                                {
                                    name     : 'The name of the game',
                                    location : users[2].location,
                                    user_id  : users[2].id
                                },
                                {
                                    name     : 'The gym brawl',
                                    location : users[3].location,
                                    user_id  : users[3].id
                                }
                            ],

                            {
                                // Emits hooks for each create, as well as the bulk create hook
                                individualHooks: true
                            }

                        ).then(function() {

                            Game.findAll().then(function(games) {

                                GameWeapon.bulkCreate([
                                    {
                                        game_id   : games[0].id,
                                        weapon_id : weapons[0].id
                                    },
                                    {
                                        game_id   : games[0].id,
                                        weapon_id : weapons[1].id
                                    },
                                    {
                                        game_id   : games[0].id,
                                        weapon_id : weapons[2].id
                                    },
                                    {
                                        game_id   : games[1].id,
                                        weapon_id : weapons[0].id
                                    },
                                    {
                                        game_id   : games[1].id,
                                        weapon_id : weapons[2].id
                                    }
                                ]).then(function() {

                                    GameUser.bulkCreate([
                                        {
                                            user_id : users[0].id,
                                            game_id : games[0].id
                                        },
                                        {
                                            user_id : users[1].id,
                                            game_id : games[0].id
                                        },
                                        {
                                            user_id : users[3].id,
                                            game_id : games[0].id
                                        },
                                        {
                                            user_id : users[4].id,
                                            game_id : games[0].id
                                        },
                                        {
                                            user_id : users[5].id,
                                            game_id : games[0].id
                                        }
                                    ]).then(function(gameUsers) {

                                        return true;

                                    });

                                });

                            });

                        });

                    });

                });

            });

        });

    }

};