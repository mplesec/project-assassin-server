var config        = require('config');

var Hapi          = require('hapi');
var HapiSequelize = require('hapi-sequelized');
var HapiAuthJwt   = require('hapi-auth-jwt2');
var Bell          = require('bell');
var Good          = require('good');
var GoodConsole   = require('good-console');

var RoutesWeb     = require('./src/routes/web')
var RoutesApi     = require('./src/routes/api');

var Populate      = require('./src/scripts/populate');


var server = new Hapi.Server();

var env = process.env.NODE_ENV || "development";

console.log('INFO: NODE_ENV=' + env);

if (env === 'development' || env === 'production') {

    server.connection({
        uri  : config.get('uri'),
        port : config.get('port'),
        router : {
            stripTrailingSlash : true
        }
    });

} else {

    throw new Error('ERROR: Please specify the NODE_ENV environment variable, ' +
                    'can be "development" or "production"\n');

    process.exit(1); // Exit code 1 means failure

}

// Load plugins and start the server
server.register([
    {
        register : HapiSequelize,
        options  : {

            database  : config.get('database.name'),
            user      : config.get('database.username'),
            pass      : config.get('database.password'),
            dialect   : 'mysql',
            host      : config.get('database.host'),
            port      : config.get('database.port'),
            models    : './src/models/api/**/*.js',
            logging   : config.has('sql.logging') ? console.log : false,
            sequelize : {

                define : {

                    // Use undescore (an_a_helicopter) naming instead of camelcase (anahelicopter)
                    underscored : true,

                    // No pluralization of table name
                    freezeTableName : true

                }

            }

        }
    },
    {
        register : HapiAuthJwt
    },
    {
        register : Bell
    },
    {
        register : Good,
        options  : {
            reporters : [{
                reporter : GoodConsole,
                events   : {
                    response : '*',
                    log      : '*'
                }
            }]
        }
    }
], function(err) {

    if (err) {

        // Check if database does not exist
        if (err.message.indexOf('ER_BAD_DB_ERROR') > -1) {

            console.log('WARN: Database ' + config.get('database.name') + ' must already exist.');

        }

        throw err;

        process.exit(1);

    }


    // Enables access to models via the Hapi request object
    server.ext('onPreHandler', function(models) {

        return function(request, reply) {

            request.models = models;
            reply.continue();

        }

    }( server.plugins['hapi-sequelized'].db.sequelize.models) );


    server.auth.strategy(
        'token', // Name of strategy
        'jwt',   // Name of scheme used for this strategy
        {
            key          : config.get('auth.token.secret'),
            validateFunc : require('./src/controllers/api/helpers/auth').validateToken,
            verifyOptions : {
                algorithms : [ 'HS256' ] // HMAC using SHA-256 hash algorithm
            }
        }
    );

    /*server.auth.strategy(
        'facebook',
        'bell',
        true,
        {
            provider     : 'facebook',
            password     : 'password',
            isSecure     : false,
            clientId     : 'myClientID',
            clientSecret : 'myClientSecret'
        }
    );*/

    // Adds this strategy to every route without an auth config
    server.auth.default('token');


    // Website routes
    RoutesWeb.init(server);
    // API routes
    RoutesApi.init(server, config.get('url.api'));


    var force    = config.get('database.force') || false;
    var populate = config.get('database.populate') || false;

    // Sync Sequelize models (Creates tables if they don't exist)
    server.plugins['hapi-sequelized'].db.sequelize.sync({ force : force }).then(function() {

        if (force)
            server.log('info', 'Dropped tables, created tables');

        server.start(function() {

            server.log('info', 'Server started at: ' + server.info.uri + ':' + server.info.port);


            if (force && populate) {

                // Populates the database with sample data
                Populate.init(server.plugins['hapi-sequelized'].db);

                server.log('info', 'Database populated with sample data');

            }

        });

    });

});


process.on('SIGINT', function() {

    console.log('INFO: Shutting down from SIGINT (Ctrl-C)\n');

    // Other closing procedures go here

    process.exit(); // Default exit code 0 means success

});