# Project Assassin Server
Project Assassin back-end (business logic and API).

## Prerequisites
Project runs on [Node.js](https://nodejs.org/), so make sure you have that installed. Also make sure you have [gulp.js](http://gulpjs.com/) installed.

## Setup
Clone the repository, navigate to the directory, and type:

    npm install

<br>
After the packages have been downloaded, start your MySQL server and create a database named *assassin*.
You do not need to create any tables or run any additional queries, these things are done in-code
using [Sequelize](http://docs.sequelizejs.com).

Go into the */config/* dir and in either the *development* or *production* files set the options **database.force** and **database.populate** to **true**.

Specify the NODE_ENV variable as either development or production:

    (Linux)   export NODE_ENV=development
    (Windows) set NODE_ENV=development

After that, type:

    gulp

<br>
This will run [nodemon](https://github.com/remy/nodemon), which will monitor the directory for files,
start the server, and restart it whenever a file changes.

## Config
You may configure the application in the */config/* dir files.
Most options should be left as they are, however the **database.force** and **database.populate**
options will create the database schema and populate it, and should be set to true when first
running the server.